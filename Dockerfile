# FROM golang:1.16.6-alpine3.14 as builder
FROM golang:1.16.6 as builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

ENV PORT 5000

RUN go build -o server server.go

# FROM alpine:3.14
FROM golang:1.16.6

WORKDIR /app

COPY --from=builder /app/templates ./templates
COPY --from=builder /app/sqlite ./sqlite
COPY --from=builder /app/server .

EXPOSE $PORT

CMD ["./server"]
