package controller

import (
	"gitlab.com/boangri/golang-gin-poc/dto"
	"gitlab.com/boangri/golang-gin-poc/service"
	"github.com/gin-gonic/gin"
)

type LoginController interface {
	Login(c *gin.Context) string
}

type loginController struct {
	loginService service.LoginService
	jWtService service.JWTService
}

func NewLoginController(service service.LoginService, jwtSrv service.JWTService) LoginController {
	loginService := service
	jWtService := jwtSrv
	return &loginController {
		loginService: loginService,
		jWtService: jWtService,
	}
}

func (controller *loginController) Login(ctx *gin.Context) string {
	var credentials dto.Credentials
	err := ctx.ShouldBind(&credentials)
	if err != nil {
		return ""
	}
	isAuthenticated := controller.loginService.Login(credentials.Username, credentials.Password)
	if isAuthenticated {
		return controller.jWtService.GenerateToken(credentials.Username, true)
	}
	return ""
}
