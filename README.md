# Golang / Go Gin Framework Crash Course 01

## 01 | Getting started

https://www.youtube.com/watch?v=qR0WnWL2o1Q

## 02 | Middlewares 101: Authorization, Logging and Debugging

https://www.youtube.com/watch?v=Ypwv1mFZ5vU

## 03 | Data Binding and Validation

https://www.youtube.com/watch?v=lZsbPtGfGIs

## 04 | HTML, Templates and Multi-Route Grouping

https://www.youtube.com/watch?v=sDJLQMZzzM4

## 09 | Setting up a JSON Web Token (JWT) Authorization Middleware

https://www.youtube.com/watch?v=p3maH9G_DLM


<!-- ### Hi there 👋

- 🔭 I’m currently working as a freelancer.
- 🌱 I’m currently learning Golang and DevOps.
- :book:  [Articles In Medium](https://xinu-93915.medium.com/)
- :email: Contact on xinu@yandex.ru


![enter image description here](https://github-readme-stats.vercel.app/api?username=boangri&&show_icons=true&title_color=ffffff&icon_color=bb2acf&text_color=daf7dc&bg_color=151515) -->
